import numpy as np
import cv2
import yarp

import mediapipe as mp

WIDTH = 1280  # dimensions of realsense
HEIGHT = 720


def main():
    # Initialise YARP
    yarp.Network.init()

    # Create a port
    input_port = yarp.Port()
    input_port.open("/keypointsDetector/rgbImage:i")

    # Create numpy array to receive the image and the YARP image wrapped around it
    img_array = np.zeros((HEIGHT, WIDTH, 3), dtype=np.uint8)
    yarp_image = yarp.ImageRgb()
    yarp_image.resize(WIDTH, HEIGHT)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])

    # Create output visualize port
    output_port = yarp.Port()
    output_port.open("/keypointsDetector/rgbImage:o")

    # Create output keypoints port
    keypoints_port = yarp.Port()
    keypoints_port.open("/keypointsDetector/keypoints:o")

    # Prepare mediapipe hand detector
    mp_hands = mp.solutions.hands
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    hands = mp_hands.Hands(static_image_mode=False,
                           max_num_hands=2,
                           min_detection_confidence=0.2,
                           min_tracking_confidence=0.4)

    halpeParts = ["Nose", "LEye", "REye", "LEar", "REar", "LShoulder", "RShoulder", "LElbow", "RElbow", "LWrist",
                  "RWrist", "LHip", "RHip", "LKnee", "RKnee", "LAnkle", "RAnkle", "Head", "Neck", "Hip", "LBigToe",
                  "RBigToe", "LSmallToe", "RSmallToe", "LHeel", "RHeel", "RJaw_0", "RJaw_1", "RJaw_2", "RJaw_3",
                  "RJaw_4", "RJaw_5", "Chin_0", "Chin_1", "Chin_2", "Chin_3", "Chin_4", "LJaw_5", "LJaw_4", "LJaw_3",
                  "LJaw_2", "LJaw_1", "LJaw_0", "Forehead_0", "Forehead_1", "Forehead_2", "Forehead_3", "Forehead_4",
                  "Forehead_5", "Forehead_6", "Forehead_7", "Forehead_8", "Forehead_9", "FNose_0", "FNose_1", "FNose_2",
                  "FNose_3", "FNose_4", "FNose_5", "FNose_6", "FNose_7", "FNose_8", "FREye_0", "FREye_1", "FREye_2",
                  "FREye_3", "FREye_4", "FREye_5", "FLEye_0", "FLEye_1", "FLEye_2", "FLEye_3", "FLEye_4", "FLEye_5",
                  "Lips_0", "Lips_1", "Lips_2", "Lips_3", "Lips_4", "Lips_5", "Lips_6", "Lips_7", "Lips_8", "Lips_9",
                  "Lips_10", "Lips_11", "Mouth_0", "Mouth_1", "Mouth_2", "Mouth_3", "Mouth_4", "Mouth_5", "Mouth_6",
                  "Mouth_7", "LHWrist", "LThumbCmc", "LThumbMcp", "LThumbIp", "LThumbTip", "LIndexFingerMcp",
                  "LIndexFingerPip", "LIndexFingerDip", "LIndexFingerTip", "LMiddleFingerMcp", "LMiddleFingerPip",
                  "LMiddleFingerDip", "LMiddleFingerTip", "LRingFingerMcp", "LRingFingerPip", "LRingFingerDip",
                  "LRingFingerTip", "LPinkyMcp", "LPinkyDip", "LPinkyPip", "LPinkyTip", "RHWrist", "RThumbCmc",
                  "RThumbMcp", "RThumbIp", "RThumbTip", "RIndexFingerMcp", "RIndexFingerPip", "RIndexFingerDip",
                  "RIndexFingerTip", "RMiddleFingerMcp", "RMiddleFingerPip", "RMiddleFingerDip", "RMiddleFingerTip",
                  "RRingFingerMcp", "RRingFingerPip", "RRingFingerDip", "RRingFingerTip", "RPinkyMcp", "RPinkyDip",
                  "RPinkyPip", "RPinkyTip"]

    # Functions to process input image
    def hand_detection(im):
        res = hands.process(im)
        right_hand_kpts = None
        left_hand_kpts = None
        # go through hand landmarks detected by mediapipe hand detector
        if res.multi_hand_landmarks:
            for idx, hand_landmarks in enumerate(res.multi_hand_landmarks):
                handedness = res.multi_handedness[idx]
                kpts_x = np.zeros(21)
                kpts_y = np.zeros(21)
                kpts_conf = np.zeros(21)

                # compute keypoints coordinates
                for i, landmark in enumerate(hand_landmarks.landmark):
                    x_coord = landmark.x * WIDTH
                    y_coord = landmark.y * HEIGHT

                    if 0 <= x_coord < WIDTH and 0 <= y_coord < HEIGHT:
                        kpts_x[i] = x_coord
                        kpts_y[i] = y_coord
                        kpts_conf[i] = 0.5
                if handedness.classification[0].label == "Right":
                    left_hand_kpts = np.zeros(3 * 21)
                    left_hand_kpts[0::3] = kpts_x
                    left_hand_kpts[1::3] = kpts_y
                    left_hand_kpts[2::3] = kpts_conf
                else:
                    right_hand_kpts = np.zeros(3 * 21)
                    right_hand_kpts[0::3] = kpts_x
                    right_hand_kpts[1::3] = kpts_y
                    right_hand_kpts[2::3] = kpts_conf

        return res, left_hand_kpts, right_hand_kpts

    print("Detector and ports prepared.")
    while True:
        # Read the data from the port into the image
        input_port.read(yarp_image)

        # convert numpy array image to cv2 image
        image = cv2.cvtColor(img_array, cv2.COLOR_BGR2RGB)

        hand_results, left_hand_results, right_hand_results = hand_detection(image)

        bout = yarp.Bottle()
        kpts = bout.addList()
        for result in [left_hand_results, right_hand_results]:
            if result is not None:
                for i in range(int(len(result) / 3)):
                    kpt = kpts.addList()
                    kpt.addString(halpeParts[94 + i])
                    kpt.addDouble(float(result[3 * i]))
                    kpt.addDouble(float(result[3 * i + 1]))
                    kpt.addDouble(float(result[3 * i + 2]))  # not tested

        keypoints_port.write(bout)
        # process the results (send keypoints and visualized image to ports)
        img_out = image
        if hand_results.multi_hand_landmarks:
            for hand_landmarks in hand_results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(img_out, hand_landmarks, mp_hands.HAND_CONNECTIONS,
                                          mp_drawing_styles.get_default_hand_landmarks_style(),
                                          mp_drawing_styles.get_default_hand_connections_style())
        img_array[:] = cv2.cvtColor(img_out, cv2.COLOR_RGB2BGR)
        output_port.write(yarp_image)

    # Cleanup
    input_port.close()
    output_port.close()
    keypoints_port.close()


if __name__ == '__main__':
    main()