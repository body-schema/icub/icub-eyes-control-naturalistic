import cv2
import math
import mediapipe as mp
import numpy as np
from typing import Tuple, Union

# Nose tip landmark index
NOSE_TIP_INDEX = 4

def _normalized_to_pixel_coordinates(
    normalized_x: float, normalized_y: float, image_width: int,
    image_height: int) -> Union[None, Tuple[int, int]]:
  """Converts normalized value pair to pixel coordinates."""

  # Checks if the float value is between 0 and 1.
  def is_valid_normalized_value(value: float) -> bool:
    return (value > 0 or math.isclose(0, value)) and (value < 1 or
                                                      math.isclose(1, value))

  if not (is_valid_normalized_value(normalized_x) and
          is_valid_normalized_value(normalized_y)):
    # TODO: Draw coordinates even if it's outside of the image bounds.
    return None
  x_px = min(math.floor(normalized_x * image_width), image_width - 1)
  y_px = min(math.floor(normalized_y * image_height), image_height - 1)
  return x_px, y_px

# Initialize Mediapipe Hands and Face Mesh
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode=False, max_num_hands=10, min_detection_confidence=0.5,
                      min_tracking_confidence=0.5)

mpdraw = mp.solutions.drawing_utils
mp_face_detection = mp.solutions.face_detection
face_detection = mp_face_detection.FaceDetection(min_detection_confidence=0.5)
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=5)

# Function to calculate distance between two points in 2D space
def calculate_distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# Polynomial regression coefficients for distance estimation
x = [300, 245, 200, 170, 145, 130, 112, 103, 93, 87, 80, 75, 70, 67, 62, 59, 57]
y = [20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
coff = np.polyfit(x, y, 2)  # y = Ax^2 + Bx + C

# OpenCV code to capture video from the default camera
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

# Main loop to process video frames
while True:
    isopen, frame = cap.read()
    if not isopen:
        break

    frame = cv2.flip(frame, 1)  # Flip the frame horizontally
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert BGR image to RGB for Mediapipe processing

    # Process the frame with Mediapipe Hands and Face Detection
    hand_results = hands.process(img)
    face_detection_results = face_detection.process(img)
    face_mesh_results = face_mesh.process(img)

    h, w, c = frame.shape  # Get the height, width, and number of channels of the frame

    # Process each detected hand in the frame
    if hand_results.multi_hand_landmarks:
        for handType, handLms in zip(hand_results.multi_handedness, hand_results.multi_hand_landmarks):
            # Extract landmark points and store them in lists
            xList, yList = [], []
            for id, lm in enumerate(handLms.landmark):
                px, py = int(lm.x * w), int(lm.y * h)
                xList.append(px)
                yList.append(py)

            # Calculate bounding box around the hand landmarks
            xmin, xmax = min(xList), max(xList)
            ymin, ymax = min(yList), max(yList)
            boxW, boxH = xmax - xmin, ymax - ymin
            bbox = xmin, ymin, boxW, boxH
            cx, cy = bbox[0] + (bbox[2] // 2), bbox[1] + (bbox[3] // 2)

            # Draw landmarks and bounding box on the frame
            mpdraw.draw_landmarks(frame, handLms, mpHands.HAND_CONNECTIONS)
            cv2.rectangle(frame, (bbox[0] - 20, bbox[1] - 20), (bbox[0] + bbox[2] + 20, bbox[1] + bbox[3] + 20),
                          (255, 0, 255), 2)
            cv2.putText(frame, handType.classification[0].label, (bbox[0] - 30, bbox[1] - 30), cv2.FONT_HERSHEY_PLAIN,
                        2, (255, 0, 255), 2)

            # Calculate and display the distance between two specific landmarks of the hand
            if len(xList) > 17 and len(yList) > 17:  # Check if the landmarks are detected
                try:
                    x, y = xList[5], yList[5]  # Coordinates of the index finger tip
                    x2, y2 = xList[17], yList[17]  # Coordinates of the wrist
                    pixel_distance = calculate_distance(x, y, x2, y2)

                    # Apply the polynomial regression coefficients
                    A, B, C = coff
                    distanceCM = A * pixel_distance ** 2 + B * pixel_distance + C

                    # Display the estimated distance
                    cv2.rectangle(frame, (xmax - 80, ymin - 80), (xmax + 20, ymin - 20), (255, 0, 255), cv2.FILLED)
                    cv2.putText(frame, f"{int(distanceCM)}cm", (xmax - 80, ymin - 40), cv2.FONT_HERSHEY_COMPLEX, 1,
                                (255, 255, 0), 2)
                except Exception as e:
                    print(f"Error in distance calculation: {e}")

    # Process face detection results
    if face_detection_results.detections:
        for detection in face_detection_results.detections:
            bboxC = detection.location_data.relative_bounding_box
            key_points = detection.location_data.relative_keypoints
            keypoint_px = _normalized_to_pixel_coordinates(key_points[2].x, key_points[2].y,
                                                           w, h)
            ih, iw, _ = img.shape
            x, y, w, h = int(bboxC.xmin * iw), int(bboxC.ymin * ih), int(bboxC.width * iw), int(bboxC.height * ih)
            # Draw bounding box
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.circle(frame, keypoint_px, 5, (0, 255, 0), -1)

            # Calculate and display the distance of the face (example)
            #distance = w * 15  # Adjust the multiplier according to your needs
            #cv2.putText(frame, f"{int(distance)}cm", (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    # #Face mesh detection
    # if face_mesh_results.multi_face_landmarks:
    #     for face_landmarks in face_mesh_results.multi_face_landmarks:
    #         mpdraw.draw_landmarks(frame, face_landmarks, mp_face_mesh.FACEMESH_CONTOURS)
    #
    #         # Get the coordinates of the nose tip
    #         nose_tip = face_landmarks.landmark[NOSE_TIP_INDEX]
    #         nose_tip_x, nose_tip_y = int(nose_tip.x * w), int(nose_tip.y * h)
    #         cv2.circle(frame, (nose_tip_x, nose_tip_y), 5, (0, 255, 0), -1)  # Draw a green circle at the nose tip

    # Display the frame with annotations
    cv2.imshow('MediaPipe Hands, Face Detection, and Face Mesh', frame)

    # Exit the loop if 'q' key is pressed
    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
