import yarp
import numpy as np
import cv2
import mediapipe as mp
import math

# Initialize YARP
yarp.Network.init()

# Create YARP ports
input_port = yarp.Port()
output_port = yarp.Port()
keypoints_port = yarp.Port()
input_port.open("/mediapipe/image:i")
output_port.open("/mediapipe/image:o")
keypoints_port.open("/mediapipe/keypoints:o")

# Initialize MediaPipe solutions
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode=False, max_num_hands=10, min_detection_confidence=0.5,
                      min_tracking_confidence=0.5)
mpdraw = mp.solutions.drawing_utils
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=5)


# Function to calculate distance between two points in 2D space
def calculate_distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# Function to add keypoints to a YARP bottle
def add_keypoints_to_bottle(bottle, keypoints, label_prefix):
    for id, keypoint in enumerate(keypoints.landmark):
        x, y = int(keypoint.x * frame.shape[1]), int(keypoint.y * frame.shape[0])
        keypoints_bottle = bottle.addList()
        keypoints_bottle.addString(f"{label_prefix}_{id}")
        keypoints_bottle.addDouble(x)
        keypoints_bottle.addDouble(y)

# Polynomial regression coefficients for distance estimation
x = [300, 245, 200, 170, 145, 130, 112, 103, 93, 87, 80, 75, 70, 67, 62, 59, 57]
y = [20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
coff = np.polyfit(x, y, 2)  # y = Ax^2 + Bx + C

# Prepare to receive an image
yarp_image = yarp.ImageRgb()
yarp_image.resize(1280, 720)  # Assuming 1280x720 resolution
img_array = np.zeros((720, 1280, 3), dtype=np.uint8)
yarp_image.setExternal(img_array.data, img_array.shape[1], img_array.shape[0])

while True:
    # Read the image from the port
    input_port.read(yarp_image)

    # Convert the image to OpenCV format
    frame = cv2.cvtColor(img_array, cv2.COLOR_RGB2BGR)

    # Process the frame with MediaPipe Hands and Face Mesh
    hand_results = hands.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    face_results = face_mesh.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

    h, w, c = frame.shape  # Get the height, width, and number of channels of the frame

    # Draw hand landmarks and calculate distance
    if hand_results.multi_hand_landmarks:
        for handType, handLms in zip(hand_results.multi_handedness, hand_results.multi_hand_landmarks):
            # Extract landmark points and store them in lists
            xList, yList = [], []
            for id, lm in enumerate(handLms.landmark):
                px, py = int(lm.x * w), int(lm.y * h)
                xList.append(px)
                yList.append(py)

            # Calculate bounding box around the hand landmarks
            xmin, xmax = min(xList), max(xList)
            ymin, ymax = min(yList), max(yList)
            boxW, boxH = xmax - xmin, ymax - ymin
            bbox = xmin, ymin, boxW, boxH
            cx, cy = bbox[0] + (bbox[2] // 2), bbox[1] + (bbox[3] // 2)

            # Draw landmarks and bounding box on the frame
            mpdraw.draw_landmarks(frame, handLms, mpHands.HAND_CONNECTIONS)
            cv2.rectangle(frame, (bbox[0] - 20, bbox[1] - 20), (bbox[0] + bbox[2] + 20, bbox[1] + bbox[3] + 20),
                          (255, 0, 255), 2)
            cv2.putText(frame, handType.classification[0].label, (bbox[0] - 30, bbox[1] - 30), cv2.FONT_HERSHEY_PLAIN,
                        2, (255, 0, 255), 2)

            # Calculate and display the distance between two specific landmarks of the hand
            if len(xList) > 17 and len(yList) > 17:  # Check if the landmarks are detected
                try:
                    x, y = xList[5], yList[5]  # Coordinates of the index finger tip
                    x2, y2 = xList[17], yList[17]  # Coordinates of the wrist
                    pixel_distance = calculate_distance(x, y, x2, y2)

                    # Apply the polynomial regression coefficients
                    A, B, C = coff
                    distanceCM = A * pixel_distance ** 2 + B * pixel_distance + C

                    # Display the estimated distance
                    cv2.rectangle(frame, (xmax - 80, ymin - 80), (xmax + 20, ymin - 20), (255, 0, 255), cv2.FILLED)
                    cv2.putText(frame, f"{int(distanceCM)}cm", (xmax - 80, ymin - 40), cv2.FONT_HERSHEY_COMPLEX, 1,
                                (255, 255, 0), 2)
                except Exception as e:
                    print(f"Error in distance calculation: {e}")

    # Draw face mesh landmarks
    if face_results.multi_face_landmarks:
        for face_landmark in face_results.multi_face_landmarks:
            mpdraw.draw_landmarks(frame, face_landmark, mp_face_mesh.FACEMESH_CONTOURS)

    # Convert the processed frame back to YARP format and write it to the output port
    output_yarp_image = yarp.ImageRgb()
    output_yarp_image.resize(1280, 720)
    output_yarp_image.setExternal(frame.data, frame.shape[1], frame.shape[0])
    output_port.write(output_yarp_image)

    # Send keypoints or other data through keypoints_port if necessary
    # Create a bottle for keypoints data
    keypoints_bottle = yarp.Bottle()

    # Process hand keypoints
    if hand_results.multi_hand_landmarks:
        for idx, hand_landmarks in enumerate(hand_results.multi_hand_landmarks):
            hand_label = f"Hand_{idx}"
            add_keypoints_to_bottle(keypoints_bottle, hand_landmarks, hand_label)

    # Process face keypoints
    if face_results.multi_face_landmarks:
        for idx, face_landmarks in enumerate(face_results.multi_face_landmarks):
            face_label = f"Face_{idx}"
            add_keypoints_to_bottle(keypoints_bottle, face_landmarks, face_label)

    # Send keypoints data through keypoints_port
    keypoints_port.write(keypoints_bottle)

    # Break the loop if a certain condition is met (e.g., a specific key is pressed)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Cleanup
input_port.close()
output_port.close()
keypoints_port.close()
yarp.Network.fini()
