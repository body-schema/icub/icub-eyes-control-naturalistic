# icub-eyes-control-naturalistic



## Overview

This repository contains code for controlling the eyes of the iCub robot in a naturalistic manner. The repository also contains scripts for pilot social studies.

## Installation

Clone the repository:

```
git clone https://gitlab.fel.cvut.cz/body-schema/icub/icub-eyes-control-naturalistic.git
cd icub-eyes-control-naturalistic

```
Follow the setup instructions specific to the iCub environment.
