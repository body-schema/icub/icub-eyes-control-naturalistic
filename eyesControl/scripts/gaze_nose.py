#!/usr/bin/python3
import yarp
import cv2
import getch
import mediapipe as mp

# Nose tip landmark index
NOSE_TIP_INDEX = 4
# OpenCV code to capture video from the default camera
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

def main():
    # Initialize YARP
    yarp.Network.init()

    # Initialize Mediapipe Face Mesh
    mp_face_mesh = mp.solutions.face_mesh
    face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=5)

    # Open YARP port
    outport = yarp.Port()
    outport.open('/python/gaze_targets:o')

    # Connect to YARP port
    torso_port = "/gazeCtrl/in:i"
    yarp.Network.connect(outport.getName(), torso_port)

    # Main loop to process video frames
    while True:
        isopen, frame = cap.read()
        if not isopen:
            break

        frame = cv2.flip(frame, 1)  # Flip the frame horizontally
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert BGR image to RGB for Mediapipe processing

        # Process the frame with Mediapipe Face Mesh
        face_mesh_results = face_mesh.process(img)

        h, w, c = frame.shape  # Get the height, width, and number of channels of the frame

        # Face mesh detection
        if face_mesh_results.multi_face_landmarks:
            for face_landmarks in face_mesh_results.multi_face_landmarks:
                # Get the coordinates of the nose tip
                nose_tip = face_landmarks.landmark[NOSE_TIP_INDEX]
                nose_tip_x, nose_tip_y = int(nose_tip.x * w), int(nose_tip.y * h)

                # Send nose tip coordinates to YARP port
                bot = yarp.Bottle()
                bot.addFloat64(nose_tip_x)
                bot.addFloat64(nose_tip_y)
                bot.addFloat64(1)  # Assuming z-coordinate is 0
                outport.write(bot)
                print("Sent")
                print([nose_tip_x, nose_tip_y, 0])

        # Exit the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    # Close the YARP port and the network
    outport.close()
    yarp.Network.fini()


if __name__ == "__main__":
    main()
