#!/usr/bin/python3
import yarp
import getch


def main():
    yarp.Network.init()  # Initialise YARP
    torso_port = "/gazeCtrl/in:i"
    outport = yarp.Port()
    outport.open('/python/gaze_targets:o')
    yarp.Network.connect(outport.getName(), torso_port)
    targets = [[5,5,10], [5,0,0], [-10,-5,0]]  # TODO: fill with targets

    for target in targets:
        char = getch.getch()
        if char == 'q':
            break
        bot = yarp.Bottle()
        bot.addFloat64(target[0])
        bot.addFloat64(target[1])
        bot.addFloat64(target[2])
        outport.write(bot)
        print("Sent")
        print(target)

    # close the network
    yarp.Network.fini()


if __name__ == "__main__":
    main()
