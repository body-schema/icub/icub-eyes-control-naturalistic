/*
* Copyright: (C) 2015 iCub Facility - Istituto Italiano di Tecnologia
* Authors: Matej Hoffmann <matej.hoffmann@iit.it>, Alessandro Roncone <alessandro.roncone@yale.edu>
* website: www.robotcub.org
* author websites: https://sites.google.com/site/matejhof, http://alecive.github.io
*
* Permission is granted to copy, distribute, and/or modify this program
* under the terms of the GNU General Public License, version 2 or any
* later version published by the Free Software Foundation.
*
* A copy of the license can be found at
* http://www.robotcub.org/icub/license/gpl.txt
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
* Public License for more details.
 */


#include "eyesCtrlThread.h"
#include <iostream>
#include <cmath>
#include <eigen3/Eigen/Dense>


enum {
  STATE_WAIT,
  STATE_REACH,
  STATE_IDLE
};




/*********** public methods ****************************************************************************/

eyesCtrlThread::eyesCtrlThread(int _rate, std::string _name, std::string _robot, int _verbosity) :
                                                                                                   PeriodicThread(static_cast<double>(_rate)/1000.0), name(std::move(_name)), robot(std::move(_robot)),
                                                                                                   verbosity(_verbosity), state(STATE_WAIT), jntsT(0), jntsH(0)
{
  dT=getPeriod();
}

void eyesCtrlThread::prepare_steps_for_joints(){
  steps.clear();

  for (double i = face_joint_limit_min; i <= face_joint_limit_max; i += 1) {
    steps.push_back(i);
  }

  for (double i = face_joint_limit_max - 1; i >= face_joint_limit_min; i -= 1) {
    steps.push_back(i);
  }
}

bool eyesCtrlThread::threadInit()
{
  neck = new iCubHeadCenter("right_v2");
  eyeL = new iCubEye("left_v2");
  eyeR = new iCubEye("right_v2");

  // release torso links
  neck->releaseLink(0); eyeL->releaseLink(0); eyeR->releaseLink(0);
  neck->releaseLink(1); eyeL->releaseLink(1); eyeR->releaseLink(1);
  neck->releaseLink(2); eyeL->releaseLink(2); eyeR->releaseLink(2);

  qdEyes.resize(3, 0.0);
  q0.resize(9, 0.0);
  rs_matrix = eye(4,4);
  rs_matrix.setRow(0, Vector{0.9926778672,  -0.08964789262,  0.08095620594,  -0.0372932849});
  rs_matrix.setRow(1, { 0.01472872958,  0.7550455074,  0.6555069384,  -0.09669132672});
  rs_matrix.setRow(2, {   -0.1198904352,  -0.6495148475,  0.7508373635,  0.09589647841});
  rs_matrix.setRow(3, {  0,  0,  0,  1});

  target.resize(3, 0.0);
  new_target.resize(3, 0.0);
  previousX = -1;
  previousY = -1;
  idx_of_eyelid_step = 0;

  inPort.open("/"+name+"/in:i");
  outPort.open("/"+name +"/data:o");

  if (!prepareDrivers()) return false;
  //limFace->getLimits(0, &face_joint_limit_min, &face_joint_limit_max);
  face_joint_limit_min = -1;
  face_joint_limit_max = 24;
  prepare_steps_for_joints();
  printMessage(5,"[eyesCtrlThread] threadInit() finished.\n");
  return true;
}

bool eyesCtrlThread::prepareDrivers()
{
  /*****  Drivers, interfaces, control boards etc. ***********************************************************/
  yarp::os::Property OptT;
  OptT.put("robot",  robot);
  OptT.put("part",   "torso");
  OptT.put("device", "remote_controlboard");
  OptT.put("remote", "/"+robot+"/torso");
  OptT.put("local",  "/"+name +"/torso");
  if (!drvTorso.open(OptT))
  {
    yError("[eyesCtrlThread]Could not open torso PolyDriver!");
    return false;
  }

  bool okT = false;

  if (drvTorso.isValid())
  {
    okT = drvTorso.view(encTorso);
  }
  encTorso->getAxes(&jntsT);
  encsT = yarp::sig::Vector(jntsT,0.0);

  if (!okT)
  {
    yError("[eyesCtrlThread]Problems acquiring torso interfaces!!!!");
    return false;
  }

  //////////////////////////////////////// TODO: Pro mrkani vykopirovat, jen dat face misto head
  yarp::os::Property OptH;
  OptH.put("robot",  robot);
  OptH.put("part",   "head");
  OptH.put("device", "remote_controlboard");
  OptH.put("remote", "/"+robot+"/head");
  OptH.put("local",  "/"+name +"/head");
  if (!drvHead.open(OptH))
  {
    yError("[eyesCtrlThread]Could not open head PolyDriver!");
    return false;
  }

  bool okH = false;

  if (drvHead.isValid())
  {
    okH = drvHead.view(encHead) && drvHead.view(posHead) && drvHead.view(modHead) && drvHead.view(limHead) && drvHead.view(ipos);
  }
  encHead->getAxes(&jntsH);
  encsH = yarp::sig::Vector(jntsH,0.0);

  if (!okH)
  {
    yError("[eyesCtrlThread]Problems acquiring head interfaces!!!!");
    return false;
  }

  setCtrlModes(jointsToSetEyes,"head","positionDirect");
  ////////////////////////////////////////

  /// Here may be the mistake
  yarp::os::Property OptF;
  OptF.put("robot",  robot);
  OptF.put("part",   "face");
  OptF.put("device", "remote_controlboard");
  OptF.put("remote", "/"+robot+"/face");
  OptF.put("local",  "/"+name +"/face");
  if (!drvFace.open(OptF))
  {
    yError("[eyesCtrlThread]Could not open head PolyDriver!");
    return false;
  }

  bool okF = false;

  if (drvFace.isValid())
  {
    okF = drvFace.view(encFace) && drvFace.view(posFace) && drvFace.view(modFace) && drvFace.view(limFace) && drvFace.view(iposFace);
  }
  encFace->getAxes(&jntsF);
  encsF = yarp::sig::Vector(jntsF,0.0);

  if (!okF)
  {
    yError("[eyesCtrlThread]Problems acquiring face interfaces!!!!");
    return false;
  }

  setCtrlModes(jointsToSetEyes,"face","positionDirect");
  ///

  return true;
}

void eyesCtrlThread::set_eyes_joint_step(){
  target_changed = false;
  Vector init{q0[6], q0[7], 0};
  Vector diff = {new_target[0] - init[0], new_target[1] - init[1], 0};
  float timestep = 0.02;
  float time = 1.5;
  step = (diff * timestep) / time;
  yInfo() << (std::to_string(step[0]) + " " + std::to_string(step[1]) + " " + std::to_string(diff[0])  + " " + std::to_string(diff[1]) + " " + std::to_string(init[0]) + " " + std::to_string(init[1]) + std::to_string(q0[6]) + " " + std::to_string(q0[7]));
  yInfo() << ("target " + std::to_string(new_target[0]) + " " + std::to_string(new_target[1]));
}

void eyesCtrlThread::make_step(){
  if(abs (qdEyes[0] - new_target[0]) > 1){
    qdEyes[0] += step[0];
  }
  if(abs (qdEyes[1] - new_target[1]) > 1) {
    qdEyes[1] += step[1];
  }
  if((abs(qdEyes[1] - new_target[1]) <= 1) && (abs(qdEyes[0] - new_target[0]) <= 1)){
    state = STATE_IDLE;
  }
  //yInfo() << ("qd" + std::to_string(qdEyes[0]) + std::to_string(qdEyes[1]));
}

void eyesCtrlThread::run()
{
  updateHeadChain();

  if (Bottle *xdNew=inPort.read(false)) {
    if(xdNew->size() == 1){
      // blink
      must_blink = true;
    }else if (xdNew->size() == 2){
      // joint coordinates
      new_target= {xdNew->get(0).asFloat64(), xdNew->get(1).asFloat64(), 0};
      target_changed = true;
      yInfo() << "target changed";
      state = STATE_REACH;
      got_pixels_joints = 1;
    } else if (xdNew->size() == 3){
      // pixel coordinates
      got_pixels_joints = 0;
      pixel_target = {xdNew->get(0).asFloat64(), xdNew->get(1).asFloat64(), xdNew->get(2).asFloat64()};
      setNewTarget(pixel_target);
    }
  }
  // klob. souradnice(q0 - aktualni), qdeyes(kam jede), mrkani(must_blink + index), new_target, pixel target co prijde(new_target, pozor ze je neukladam v pripade 3 souradni)(pixel1, pixel2, pixel3 nabo joint1, joint2) abych byla schopna to rozlisit, jak prisel cil
  // aby bylo videt ze ten rozdil klesa do nuly
  ts.update();
  yarp::os::Bottle b;
  b.clear();
  for(int i = 0; i < 8; ++i){
    b.addFloat64(q0[i]);
  }
  b.addFloat64(qdEyes[0]);
  b.addFloat64(qdEyes[1]);
  b.addInt32(must_blink);
  b.addInt32(idx_of_eyelid_step);
  if(got_pixels_joints == 0){ // got pixels
    b.addFloat64(pixel_target[0]);
    b.addFloat64(pixel_target[1]);
    b.addFloat64(pixel_target[2]);
  } else {
    b.addFloat64(-1);
    b.addFloat64(-1);
    b.addFloat64(-1);
  }
  b.addFloat64(new_target[0]);
  b.addFloat64(new_target[1]);
  outPort.setEnvelope(ts);
  outPort.write(b);

  //if(idx_of_eyelid_step == steps.size() && run_counter > 200){
  //run_counter = 0;
  //}

  if(must_blink){ // here was (run_counter >= 200)
    if(idx_of_eyelid_step == steps.size()){
      idx_of_eyelid_step = 0;
      must_blink = false;
    }
    printf("Idx of step: %d\n", idx_of_eyelid_step);
    controlFace("positionDirect");
  }

  switch (state)
  {
  case STATE_WAIT:
  {
    break;
  }
  case STATE_REACH:
  {
    Vector eye_pose = eyeL->EndEffPose();
    Vector eye_p = eye_pose.subVector(0,2);
    Vector eye_o = eye_pose.subVector(3,6);  // angle axis representation (first three elements are the axis, and the last one is the angle)
    Matrix torso_eye = axis2dcm(eye_o);
    torso_eye.setSubcol(eye_p, 0, 3);  // rotation matrix representation
    Vector neck_pose = neck->EndEffPose();
    Vector neck_p = neck_pose.subVector(0,2);
    Vector neck_o = neck_pose.subVector(3,6);  // angle axis representation (first three elements are the axis, and the last one is the angle)
    Matrix torso_neck = axis2dcm(neck_o);  // rotation matrix representation
    torso_neck.setSubcol(neck_p, 0, 3);
    Matrix torso_rs = torso_neck * rs_matrix;



    // TODO: davat te kroky a mezikroky postupne do qdEyes, aby tam nejel rovnou na cil a pak tam kontrolovat toho cloveka po kazdem kroku
    // bud kroky nebo zpocitavat kde je clovek a kam mam jet, coz znamena ze mam opravovat ten cil vzdy
    // kontrolovat ze ten rozdil mezi cilem je vetsi nez krok(nebo konstanta)
    if(target_changed){
      set_eyes_joint_step();
    }
    make_step();
    //qdEyes = target;
    controlHead("positionDirect");
    updateHeadChain(); // tady bude kontrola jestli target == qo(kloubove souradnece prectene z robota) (souradnice jsou absolutni, 0 je vzdy 0)

    //    bool targetReached = true; // if desired position reached, switch to STATE_IDLE
    //    if (targetReached)
    //    {
    //      state = STATE_IDLE;
    //    }
    break;
  }
  case STATE_IDLE:
  {
    state = STATE_WAIT;
    break;
  }
  default:
    yFatal("[eyesCtrlThread] eyesCtrlThread should never be here!!! Step: %d",state);
  }
  printMessage(2,"[eyesCtrlThread::run()] finished, state: %d.\n\n\n",state);
  run_counter++;
}


void eyesCtrlThread::threadRelease()
{
  yInfo("threadRelease(): deleting arm and torso encoder arrays and arm object.");
  const bool stoppedOk = stopControlAndSwitchToPositionMode();
  if (stoppedOk) { yInfo("Successfully stopped arm and torso controllers"); }
  else { yWarning("Controllers not stopped successfully"); }
  yInfo("Closing controllers..");
  drvHead.close();
  drvTorso.close();
  inPort.interrupt();
  inPort.close();
  outPort.interrupt();
  outPort.close();
}


bool eyesCtrlThread::setNewTarget(const Vector& _x_d)
{
  // TODO: here will be target calculating
  int targetX = _x_d[0]; //172;
  int targetY = _x_d[1]; //224;

  // TODO: if target is not so different from the previous one, go to state idle and return true, ignore the rest
  if((previousX != -1 && abs(previousX - targetX) <= 5) && (previousY != -1 && abs(previousY - targetY) <= 5)){
    state = STATE_IDLE;
    return true;
  }

  int centerX = 320;
  int centerY = 240;
  int resolutionX = 640; // Not used in this calculation, provided for context
  int resolutionY = 480; // Not used in this calculation, provided for context

  // Displacement from center
  double deltaX = centerX - targetX; // Pixel displacement X
  double deltaY = centerY - targetY; // Pixel displacement Y

  // Right  camera matrix
  double fx = 446.307;
  double fy = 448.447;
  double cx = 313.734;
  double cy = 224.469;

  Eigen::MatrixXd A(3, 3);
  A <<  fx, 0, cx,
      0, fy, cy,
      0, 0, 1;

  // Calculate the inverse of matrix A
  Eigen::MatrixXd A_inv = A.inverse();

  Eigen::VectorXd v(3);
  v << deltaX, 0, 1;

  // Multiply vector v by the inverse of matrix A
  Eigen::VectorXd resultX = A_inv * v;

  Eigen::VectorXd u(3);
  u << 0, deltaY, 1;

  // Multiply vector v by the inverse of matrix A
  Eigen::VectorXd resultY = A_inv * u;

  // Test
  Eigen::VectorXd stred(3);
  stred << centerX, centerY, 1;

  Eigen::VectorXd pos(3);
  pos << targetX, targetY, 1;

  // Multiply vector v by the inverse of matrix A
  Eigen::VectorXd resultPos = A_inv * pos;
  Eigen::VectorXd resultStred = A_inv * stred;


  // Calculate angular displacement in radians
  double thetaXRad = atan2(resultPos.coeff(1), resultPos.coeff(2)); // atan2 considers the sign and quadrant
  double thetaYRad = atan2(resultPos.coeff(0), resultPos.coeff(2));

  // Convert radians to degrees
  double thetaXDeg = -(thetaXRad * (180.0 / M_PI));
  double thetaYDeg = thetaYRad * (180.0 / M_PI);

  // Correcting the angles based on the physical constraints of the robot's eyes (-30 to 30 degrees)
  double min_valueX;
  double max_valueX;
  limHead->getLimits(3, &min_valueX, &max_valueX);
  thetaXDeg = std::max(min_valueX + 2, std::min(max_valueX - 2, thetaXDeg));

  double min_valueY;
  double max_valueY;
  limHead->getLimits(4, &min_valueY, &max_valueY);
  thetaYDeg = std::max(min_valueY + 2, std::min(max_valueY - 2, thetaYDeg));

  // Assigning to target and qdEyes considering the correct axes
  new_target= {q0[6] + thetaXDeg, q0[7] + thetaYDeg, 0}; // Assuming first value is for horizontal, second for vertical
  target_changed = true;
  yInfo() << "target changed";
  //qdEyes[0] = thetaXDeg; // Horizontal movement angle (yaw)
  //qdEyes[1] = thetaYDeg; // Vertical movement angle (pitch)
  //qdEyes[2] = 0; // Assuming no roll movement is required

  //target= {_x_d[0], _x_d[1], _x_d[2]};
  //yInfo() << _x_d.toString();
  //yInfo() << (std::to_string(thetaXDeg) + " " + std::to_string(thetaYDeg) + " " + std::to_string(resultStred.coeff(0)) + " " + std::to_string(resultStred.coeff(1))+ " " + std::to_string(resultStred.coeff(2))
  // + " " + std::to_string(resultPos.coeff(0)) + " " + std::to_string(resultPos.coeff(1))+ " " + std::to_string(resultPos.coeff(2)) + " " +  std::to_string(resultX.coeff(0)) + " " +  std::to_string(resultY.coeff(1)));
  state = STATE_REACH;
  return true;
}

void eyesCtrlThread::updateHeadChain() {
  encTorso->getEncoders(encsT.data());
  encHead->getEncoders(encsH.data());

  q0[0]=encsT[2];  // On purpose, torso joint values are in different order than links
  q0[1]=encsT[1];
  q0[2]=encsT[0];
  for (int i = 0; i < 6; i++)
  {
    q0[3+i] = encsH[i];
  }

  neck->setAng(q0.subVector(0,5)*iCub::ctrl::CTRL_DEG2RAD);
  eyeR->setAng(q0.subVector(0,5)*iCub::ctrl::CTRL_DEG2RAD);
  eyeL->setAng(q0.subVector(0,5)*iCub::ctrl::CTRL_DEG2RAD);
  eyeR->setAng(3+3,encsH[3]*iCub::ctrl::CTRL_DEG2RAD);
  eyeR->setAng(3+4,encsH[4]-encsH[5]/2.0*iCub::ctrl::CTRL_DEG2RAD);
  eyeL->setAng(3+3,encsH[3]*iCub::ctrl::CTRL_DEG2RAD);
  eyeL->setAng(3+4,encsH[4]+encsH[5]/2.0*iCub::ctrl::CTRL_DEG2RAD);

}

bool eyesCtrlThread::stopControlAndSwitchToPositionMode()
{
  state=STATE_WAIT;
  return  setCtrlModes(jointsToSetEyes,"head","position");
}


// THIS
bool eyesCtrlThread::areJointsHealthyAndSet(std::vector<int> &jointsToSet, const std::string &_p, const std::string &_s)
{
  jointsToSet.clear();
  std::vector<int> modes(3);
  if (_p=="head")
  {
    modes.resize(3,VOCAB_CM_IDLE);
    modHead->getControlModes(3, jointsToSetEyes.data(), modes.data());
  }
  else
  {
    return false;
  }

  for (int i=0; i<modes.size(); i++)
  {
    if ((modes[i]==VOCAB_CM_HW_FAULT) || (modes[i]==VOCAB_CM_IDLE)) { return false; }

    // we will set only those that are not in correct modes already
    if ((_s=="velocity" && modes[i]!=VOCAB_CM_MIXED && modes[i]!=VOCAB_CM_VELOCITY)
        || (_s=="position" && modes[i]!=VOCAB_CM_MIXED && modes[i]!=VOCAB_CM_POSITION)
        || (_s=="positionDirect" && modes[i]!=VOCAB_CM_POSITION_DIRECT))
    {
      jointsToSet.push_back(i+3);
    }
  }
  if(verbosity >= 10){
    printf("[eyesCtrlThread::areJointsHealthyAndSet] %s: ctrl Modes retrieved: ",_p.c_str());
    for (const int mode : modes){
      printf("%s ",Vocab32::decode(mode).c_str());
    }
    printf("\n");
    printf("Indexes of joints to set: ");
    for (const int joint : jointsToSet){
      printf("%d ",joint);
    }
    printf("\n");
  }

  return true;
}

bool eyesCtrlThread::areJointsHealthyAndSetFace(const std::string &_s)
{
  int mode;
  modFace->getControlMode(0, &mode);

  if ((mode==VOCAB_CM_HW_FAULT) || (mode==VOCAB_CM_IDLE)) { return false; }

  return true;
}

// THIS
bool eyesCtrlThread::setCtrlModes(const std::vector<int> &jointsToSet, const std::string &_p, const std::string &_s)
{
  if (_s!="position" && _s!="velocity" && _s!="positionDirect") { return false; }

  if (jointsToSet.empty()) { return true; }

  std::vector<int> modes;
  for (size_t i=0; i<jointsToSet.size(); i++)
  {
    if (_s=="position")
    {
      modes.push_back(VOCAB_CM_POSITION);

      // set up the speed in [deg/s]
      ipos->setRefSpeed(i,30.0);

      // set up max acceleration in [deg/s^2]
      ipos->setRefAcceleration(i,100.0);

    }
    else if (_s=="velocity")
    {
      modes.push_back(VOCAB_CM_VELOCITY);
    }
    else if (_s=="positionDirect")
    {
      modes.push_back(VOCAB_CM_POSITION_DIRECT);
    }
  }

  if (_p=="head")
  {
    modHead->setControlModes(static_cast<int>(jointsToSet.size()), jointsToSet.data(), modes.data());
  }
  else
  {
    return false;
  }
  return true;
}

bool eyesCtrlThread::setCtrlModesFace(const std::string &_s)
{
  if (_s!="position" && _s!="velocity" && _s!="positionDirect") { return false; }

  int mode;
  if (_s=="position")
  {
    mode = VOCAB_CM_POSITION;

    // set up the speed in [deg/s]
    iposFace->setRefSpeed(0,30.0);

    // set up max acceleration in [deg/s^2]
    iposFace->setRefAcceleration(0,100.0);

  }
  else if (_s=="velocity")
  {
    mode = VOCAB_CM_VELOCITY;
  }
  else if (_s=="positionDirect")
  {
    mode = VOCAB_CM_POSITION_DIRECT;
  }

  modFace->setControlMode(0, mode);

  return true;
}

// THIS
bool eyesCtrlThread::controlHead(const std::string& _controlMode)
{
  std::vector<int> jointsToSet;

  if (!areJointsHealthyAndSet(jointsToSet,"head",_controlMode))
  {
    yWarning("[eyesCtrlThread::controlArm] Stopping control because neck joints are not healthy!");
    return false;
  }

  if (!setCtrlModes(jointsToSet,"head",_controlMode))
  {
    yError("[eyesCtrlThread::controlArm] I am not able to set the neck joints to %s mode!",_controlMode.c_str());
    return false;
  }

  if(_controlMode == "positionDirect")
  {
    posHead->setPositions(static_cast<int>(jointsToSetEyes.size()), jointsToSetEyes.data(), qdEyes.data());
  }

  if(_controlMode == "position") // pokud je position == zablokovany !!!
  {
    ipos->positionMove(static_cast<int>(jointsToSetEyes.size()), jointsToSetEyes.data(),qdEyes.data());
    // wait (with timeout) until the movement is completed
    bool done=false;
    double t0=Time::now();
    while (!done&&(Time::now()-t0<10.0))
    {
      yInfo()<<"Waiting...";
      Time::delay(0.1);   // release the quantum to avoid starving resources
      ipos->checkMotionDone(static_cast<int>(jointsToSetEyes.size()), jointsToSetEyes.data(), &done);
    }

    if (done)
      yInfo()<<"Movement completed";
    else
      yWarning()<<"Timeout expired";
  }

  return true;
}

bool eyesCtrlThread::controlFace(const std::string& _controlMode)
{

  if (!areJointsHealthyAndSetFace(_controlMode))
  {
    yWarning("[eyesCtrlThread::controlArm] Stopping control because neck joints are not healthy!");
    return false;
  }

  if (!setCtrlModesFace(_controlMode))
  {
    yError("[eyesCtrlThread::controlArm] I am not able to set the face joints to %s mode!",_controlMode.c_str());
    return false;
  }


  if(_controlMode == "positionDirect")
  {
    posFace->setPosition(0, steps[idx_of_eyelid_step++]);
  }

  if(_controlMode == "position")
  {
    iposFace->positionMove(0, steps[idx_of_eyelid_step++]);
    // wait (with timeout) until the movement is completed
    bool done=false;
    double t0=Time::now();
    while (!done&&(Time::now()-t0<10.0))
    {
      yInfo()<<"Waiting...";
      Time::delay(0.1);   // release the quantum to avoid starving resources
      iposFace->checkMotionDone(0, &done);
    }

    if (done)
      yInfo()<<"Movement completed";
    else
      yWarning()<<"Timeout expired";
  }

  return true;
}

int eyesCtrlThread::printMessage(const int l, const char *f, ...) const
{
  if (verbosity>=l)
  {
    fprintf(stdout,"[%s] ",name.c_str());

    va_list ap;
    va_start(ap,f);
    const int ret=vfprintf(stdout,f,ap);
    va_end(ap);
    return ret;
  }
  return -1;
}

// empty line to make gcc happy
