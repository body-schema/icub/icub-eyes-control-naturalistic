/**
\defgroup eyesController eyesController

A module able to do stuff.

\section parameters_sec Parameters

--context    \e path
- Where to find the called resource.

--from       \e from
- The name of the .ini file with the configuration parameters.

--name       \e name
- The name of the module (default eyesController).

--robot      \e rob
- The name of the robot (either "icub" or "icubSim"). Default icubSim.

--rate       \e rate
- The period used by the thread. Default 100ms.

--verbosity  \e verb
- Verbosity level (default 0). The higher is the verbosity, the more
  information is printed out.

*/

#include <yarp/os/RFModule.h>

#include "eyesCtrlThread.h"

using namespace yarp;
using namespace yarp::os;
//using namespace yarp::math;


/**
* \ingroup eyesController
*
*  
*/
class eyesController: public RFModule
{
private:
    eyesCtrlThread *ctrlThrd;
    RpcServer            rpcSrvr;

    std::string robot;       // Name of the robot
    std::string  name;       // Name of the module

    int ctrlRate;  // rate of the thread
    int verbosity;
        
public:
    eyesController()
    {
        ctrlThrd=nullptr;
    
        //robot = "icubSim";
        robot = "icub";
        name = "gazeCtrl";
        ctrlRate = 20;
        verbosity = 1;
    }

    bool set_xd(const yarp::sig::Vector& _xd)
    {
        if (_xd.size()>=3)
        {
            yInfo(" ");
            yInfo("[eyesController] received new x_d: %s", _xd.toString(3,3).c_str());
            return ctrlThrd->setNewTarget(_xd);
        }
        return false;
    }

    bool stop()
    {
        yInfo("[eyesController] Stopping control by going to position mode..");
        return ctrlThrd->stopControlAndSwitchToPositionMode();
    }

    bool configure(ResourceFinder &rf) override
    {
        //******************************************************
        //******************* NAME ******************
            if (rf.check("name"))
            {
                name = rf.find("name").asString();
                yInfo("[eyesController] Module name set to %s", name.c_str());
            }
            else 
            {
                yInfo("[eyesController] Module name set to default, i.e. %s", name.c_str());
            }
            setName(name.c_str());

        //******************* ROBOT ******************
            if (rf.check("robot"))
            {
                robot = rf.find("robot").asString();
                yInfo("[eyesController] Robot is: %s", robot.c_str());
            }
            else 
            {
                yInfo("[eyesController] Could not find robot option in the config file; using %s as default",robot.c_str());
            }

         //******************* VERBOSE ******************
            if (rf.check("verbosity"))
            {
                verbosity = rf.find("verbosity").asInt32();
                yInfo("[eyesController] verbosity set to %i", verbosity);
            }
            else 
            {
                yInfo("[eyesController] Could not find verbosity option in the config file; using %i as default",verbosity);
            }

        //****************** rctCtrlRate ******************
            if (rf.check("ctrlRate"))
            {
                ctrlRate = rf.find("ctrlRate").asInt32();
                yInfo("[eyesController] ctrlThread working at %i ms.",ctrlRate);
            }
            else 
            {
                yInfo("[eyesController] Could not find rctCtrlRate in the config file; using %i as default",ctrlRate);
            }

             //************* THREAD ******************************
        
            
        ctrlThrd = new eyesCtrlThread(ctrlRate, name, robot, verbosity);
        if (!ctrlThrd->start())
        {
            delete ctrlThrd;
            ctrlThrd = nullptr;
            yError("[eyesController] gazeCtrlThread wasn't instantiated!!");
            return false;
        }

        rpcSrvr.open("/"+name+"/rpc:i");
        attach(rpcSrvr);

        return true;
    }

    /************************************************************************/
    bool close() override
    {
        yInfo("GAZE CONTROLLER: Stopping threads..");
        if (ctrlThrd)
        {
            yInfo("GAZE CONTROLLER: Stopping ctrlThrd...");
            ctrlThrd->stop();
            delete ctrlThrd;
            ctrlThrd=nullptr;
        }
        rpcSrvr.close();

        return true;
    }

    double getPeriod() override  { return 1.0; }
    bool updateModule() override { return true; }
};

/**
* Main function.
*/
int main(int argc, char * argv[])
{
    yarp::os::Network yarp;

    ResourceFinder rf;
    rf.setDefaultContext("react-control");
    rf.setDefaultConfigFile("eyesController.ini");
    rf.configure(argc,argv);

    if (rf.check("help"))
    {   
        yInfo(" "); 
        yInfo("Options:");
        yInfo(" ");
        yInfo("   --context     path:  where to find the called resource");
        yInfo("   --from        from:  the name of the .ini file.");
        yInfo("   --name        name:  the name of the module (default eyesController).");
        yInfo("   --robot       robot: the name of the robot. Default icubSim.");
        yInfo("   --part        part:  the arm to use. Default left_arm.");
        yInfo("   --rate        rate:  the period used by the thread. Default 100ms.");
        yInfo("   --verbosity   int:   verbosity level (default 0).");
        yInfo(" ");
        return 0;
    }
    
    if (!yarp.checkNetwork())
    {
        yError("No Network!!!");
        return -1;
    }

    eyesController rctCtrl;
    return rctCtrl.runModule(rf);
}
// empty line to make gcc happy
