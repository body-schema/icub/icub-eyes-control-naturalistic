/*
* Copyright: (C) 2015 iCub Facility - Istituto Italiano di Tecnologia
* Authors: Matej Hoffmann <matej.hoffmann@iit.it>, Alessandro Roncone <alessandro.roncone@yale.edu>
* website: www.robotcub.org
* author websites: https://sites.google.com/site/matejhof, http://alecive.github.io
*
* Permission is granted to copy, distribute, and/or modify this program
* under the terms of the GNU General Public License, version 2 or any
* later version published by the Free Software Foundation.
*
* A copy of the license can be found at
* http://www.robotcub.org/icub/license/gpl.txt
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
* Public License for more details.
*/


#ifndef __EYESCONTROLLERTHREAD_H__
#define __EYESCONTROLLERTHREAD_H__

#include <yarp/os/LogStream.h>
#include <yarp/dev/PolyDriver.h>
#include <yarp/os/PeriodicThread.h>
#include <iCub/iKin/iKinFwd.h>
#include <iCub/ctrl/minJerkCtrl.h>
#include <cstdarg>


using namespace yarp::dev;
using namespace yarp::sig;
using namespace yarp::os;
using namespace iCub::iKin;
using namespace yarp::math;

class eyesCtrlThread: public yarp::os::PeriodicThread
{

public:
 // CONSTRUCTOR
 eyesCtrlThread(int _rate, std::string _name, std::string _robot, int _verbosity);
 // INIT
 bool threadInit() override;
 // RUN
 void run() override;
 // RELEASE
 void threadRelease() override;

 // Sets the new target
 bool setNewTarget(const yarp::sig::Vector& _x_d);

 // Stops the control of the robot
 bool stopControlAndSwitchToPositionMode();

protected:
 /***************************************************************************/
 // EXTERNAL VARIABLES: change them from command line or through .ini file
 // Flag that manages verbosity (v=1 -> more text printed out; v=2 -> even more text):
 int verbosity;
 // Name of the module (to change port names accordingly):
 std::string name;
 // Name of the robot (to address the module toward icub or icubSim):
 std::string robot;
 // Flag to know if the torso shall be used or not

 /***************************************************************************/
 // INTERNAL VARIABLES:
 double dT;  //period of the thread in seconds  =getPeriod();

 int state;        // Flag to know in which state the thread is in

 // "Classical" interfaces
 // TODO dat jeste face, jako head
 iCubHeadCenter     *neck{nullptr};
 iCubEye            *eyeL{nullptr}, *eyeR{nullptr};
 PolyDriver         drvTorso,  drvHead;
 IControlMode       *modHead{nullptr};
 IPositionDirect    *posHead{nullptr};
 IEncoders          *encHead{nullptr}, *encTorso{nullptr};
 IControlLimits     *limHead{nullptr};
 IPositionControl *ipos{nullptr};

 ///Here may be the mistake
 PolyDriver         drvFace;
 IControlMode       *modFace{nullptr};
 IPositionDirect    *posFace{nullptr};
 IEncoders          *encFace{nullptr};
 IControlLimits     *limFace{nullptr};
 IPositionControl *iposFace{nullptr};

 int previousX, previousY;
 int idx_of_eyelid_step;
 double face_joint_limit_min, face_joint_limit_max;
 //0.. 60 po 5
 std::vector<int> steps;
 std::vector<int> steps_big{0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 0};
 std::vector<int> steps_small{
     0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
     16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
     30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
     44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
     58, 59, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49,
     48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35,
     34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21,
     20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6,
     5, 4, 3, 2, 1, 0
 };
 int run_counter = 0;
 int got_pixels_joints = -1; // 0 pixels, 1 joints
 Vector pixel_target;
 ///

 BufferedPort<Bottle> inPort;
 yarp::os::Port outPort;
 // Stamp for the setEnvelope for the ports
 yarp::os::Stamp ts;

 Vector qddeg,qdeg,vdeg;
 Vector v,vNeck;
 Vector q0,qd,qdEyes;
 Vector encsT, encsH, encsF;

 Matrix rs_matrix;

 Vector target;
 ///////
 Vector new_target;
 Vector step;
 bool target_changed = false;
 bool must_blink = true;
 int eyes_joint_step_x, eyes_joint_step_y; // 0, -1 or 1 depending on the direction
 /////

 int jntsT, jntsH, jntsF;

 std::vector<int> jointsToSetEyes{3,4,5};

 /**
   * Updates the head's kinematic chain with the encoders from the robot
   **/
 void updateHeadChain();

 /**
   * Sends the computed velocities or positions to the robot, depending on controlMode
  */
 bool controlHead(const std::string& controlMode);

 /**
    * Check the state of each joint to be controlled
    * @param  jointsToSet vector of integers that defines the joints to be set
    * @param  _p part to set. It can be either "torso" or "arm"
    * @param  _s mode to set. It can be either "position" or "velocity"
    * @return             true/false if success/failure
  */
 bool areJointsHealthyAndSet(std::vector<int> &jointsToSet, const std::string &_p, const std::string &_s);

 /**
    * Changes the control modes of the torso to either position or velocity
    * @param  _p part to set. It can be either "torso" or "arm"
    * @param  _s mode to set. It can be either "position" or "velocity"
    * @return    true/false if success/failure
  */
 bool setCtrlModes(const std::vector<int> &jointsToSet,
                   const std::string &_p, const std::string &_s);


 bool prepareDrivers();
 /**
   * Prints a message according to the verbosity level:
   * @param l will be checked against the global var verbosity: if verbosity >= l, something is printed
   * @param f is the text. Please use c standard (like printf)
  */
 int printMessage(int l, const char *f, ...) const;

 bool controlFace(const std::string& _controlMode);
 bool setCtrlModesFace(const std::string &_s);
 bool areJointsHealthyAndSetFace(const std::string &_s);
 void make_step();
 void set_eyes_joint_step();
 void prepare_steps_for_joints();

};

#endif
