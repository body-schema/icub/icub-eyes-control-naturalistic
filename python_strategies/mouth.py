import yarp
import time
import cv2

emotion_port = yarp.Port()
emotion_out = "/icub/face/emotions/in"

in_port = yarp.Port()
in_out = "/python/say_now:o"


def open_mouth():
    bot = yarp.Bottle()
    bot.clear()
    bot.addString("set")
    bot.addString("mou")
    bot.addString("sur")
    emotion_port.write(bot)

def close_mouth():
    bot = yarp.Bottle()
    bot.clear()
    bot.addString("set")
    bot.addString("mou")
    bot.addString("neu")
    emotion_port.write(bot)

def say_mouth():
    open_mouth()
    curr = time.time()
    while time.time() < curr + 0.4:
        pass
    close_mouth()

def main():
    # Initialize YARP
    yarp.Network.init()
    emotion_port.open("/python/emotions:o")
    yarp.Network.connect(emotion_port.getName(), emotion_out)

    in_port.open("/python/say_instructions:i")
    yarp.Network.connect(in_out, in_port.getName())
    while True:
        bot = yarp.Bottle()
        in_port.read(bot)
        if bot:
            say_mouth()
        # Exit the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xff == ord('q'):
            break



if __name__ == "__main__":
    main()