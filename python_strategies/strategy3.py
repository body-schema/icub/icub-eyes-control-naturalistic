# Strategy 3 - random movements every 5 sec
import numpy as np
import yarp
import time
import random

# Size of the frame
FRAME_WIDTH = 600
FRAME_HEIGHT = 450
FRAME_WIDTH_MIN = 20
FRAME_HEIGHT_MIN = 20

def main():
    # Initialize YARP
    yarp.Network.init()

    # Open YARP port
    outport = yarp.Port()
    outport.open('/python/gaze_targets:o')
    imagePort = yarp.BufferedPortImageRgb()
    imagePort.open('/mp/image:i')
    image_w = 640
    image_h = 480
    in_buf_array = np.ones((image_h, image_w, 3), dtype=np.uint8)
    in_buf_image = yarp.ImageRgb()
    in_buf_image.resize(image_w, image_h)
    in_buf_image.setExternal(in_buf_array, in_buf_array.shape[1], in_buf_array.shape[0])

    # Connect to YARP port
    torso_port = "/gazeCtrl/in:i"
    yarp.Network.connect(outport.getName(), torso_port)
    yarp.Network.connect('/icub/camcalib/left/out', imagePort.getName())

    output_port = yarp.Port()
    output_port.open("/mediapipe/rgbImage:o")

    img_array = np.zeros((image_h, image_w, 3), dtype=np.uint8)
    yarp_image = yarp.ImageRgb()
    yarp_image.resize(image_w, image_h)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])

    # Random pixel coordinates
    def random_pixel():
        return random.randint(-25, 10), random.randint(-20, 20)

    # Main loop to process video frames
    while True:
        # Generate random pixel location
        random_x, random_y = random_pixel()

        # Send random pixel coordinates to YARP port
        bot = yarp.Bottle()
        bot.addFloat64(random_x)
        bot.addFloat64(random_y)
        outport.write(bot)
        print("Sent:", [random_x, random_y, 1])

        # Wait for a while before sending the next random location
        for k in range(50):
            time.sleep(0.1)
            received_image = imagePort.read()
            output_port.write(received_image)

    # Close the YARP port and the network
    outport.close()
    imagePort.close()
    yarp.Network.fini()


if __name__ == "__main__":
    main()
