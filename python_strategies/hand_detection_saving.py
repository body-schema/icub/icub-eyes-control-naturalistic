# !/usr/bin/python3
from typing import Union, Tuple

import yarp
import cv2
import getch
import mediapipe as mp
import numpy as np
import time
import random
import math

LONG_WAIT = 20
SHORT_WAIT = 10
NO_WAIT = 0

CENTER_X = 320
CENTER_Y = 240
IMAGE_W = 640
IMAGE_H = 480

DETECTED_FACE = 0
DETECTED_BODY = 1
DETECTED_HAND = 2

outport = yarp.Port()
imagePort = yarp.BufferedPortImageRgb()
output_port = yarp.Port()
torso_port = "/gazeCtrl/in:i"
prev_target = None
f = open("hand_locations_0.txt", "w")

in_buf_array = np.ones((IMAGE_H, IMAGE_W, 3), dtype=np.uint8)
in_buf_image = yarp.ImageRgb()

img_array = np.zeros((IMAGE_H, IMAGE_W, 3), dtype=np.uint8)
yarp_image = yarp.ImageRgb()

# Initialize Mediapipe Face Mesh
mpdraw = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode=False, max_num_hands=10, min_detection_confidence=0.5,
                      min_tracking_confidence=0.5)

# Function to calculate distance between two points in 2D space
def calculate_distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# Polynomial regression coefficients for distance estimation
x = [300, 245, 200, 170, 145, 130, 112, 103, 93, 87, 80, 75, 70, 67, 62, 59, 57]
y = [20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
coff = np.polyfit(x, y, 2)  # y = Ax^2 + Bx + C


def init():
    # Initialize YARP
    yarp.Network.init()

    # Open YARP port
    outport.open('/python/gaze_targets:o')
    imagePort.open('/mp/image:i')
    in_buf_image.resize(IMAGE_W, IMAGE_H)
    in_buf_image.setExternal(in_buf_array, in_buf_array.shape[1], in_buf_array.shape[0])

    # Connect to YARP port
    yarp.Network.connect(outport.getName(), torso_port)
    yarp.Network.connect('/icub/camcalib/right/out', imagePort.getName())

    output_port.open("/mediapipe/rgbImage:o")

    yarp_image.resize(IMAGE_W, IMAGE_H)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])


def close_ports():
    # Close the YARP port and the network
    outport.close()
    imagePort.close()
    yarp.Network.fini()
    f.close()


def send_image_yarp(img_out):
    img_array[:] = cv2.cvtColor(img_out, cv2.COLOR_RGB2BGR)
    output_port.write(yarp_image)


def wait(timer):
    # Wait for a while before sending the next random location
    for k in range(timer):
        time.sleep(0.1)
        received_image = imagePort.read()
        output_port.write(received_image)


def send_info(info, timer):
    bot = yarp.Bottle()
    for obj in info:
        bot.addFloat64(obj)
    outport.write(bot)
    bot.clear()
    wait(timer)

def send_zero():
    send_info([CENTER_X, CENTER_Y, 0], LONG_WAIT)
    print("Sent zero")


def follow_hand(timer):
    for k in range(timer):
        received_image = imagePort.read()
        in_buf_image.copy(received_image)
        image = cv2.cvtColor(in_buf_array, cv2.COLOR_BGR2RGB)

        hand_results = hands.process(image)

        h, w, c = image.shape  # Get the height, width, and number of channels of the frame
        img_out = image

        hand_landmark = None
        hand_landmark_world = None

        # Process each detected hand in the frame
        if hand_results.multi_hand_landmarks:
            for handType, handLms in zip(hand_results.multi_handedness, hand_results.multi_hand_landmarks):
                # Extract landmark points and store them in lists
                xList, yList = [], []
                for id, lm in enumerate(handLms.landmark):
                    px, py = int(lm.x * w), int(lm.y * h)
                    xList.append(px)
                    yList.append(py)

                # Draw landmarks and bounding box on the frame
                mpdraw.draw_landmarks(img_out, handLms, mpHands.HAND_CONNECTIONS)
                if handType.classification[0].score > 0.3:
                    hand_landmark = [xList[5], yList[5]]

                # Calculate and display the distance between two specific landmarks of the hand
                if len(xList) > 17 and len(yList) > 17:  # Check if the landmarks are detected
                    try:
                        x, y = xList[5], yList[5]  # Coordinates of the index finger tip
                        x2, y2 = xList[17], yList[17]  # Coordinates of the wrist
                        pixel_distance = calculate_distance(x, y, x2, y2)

                        # Apply the polynomial regression coefficients
                        A, B, C = coff
                        distanceCM = A * pixel_distance ** 2 + B * pixel_distance + C

                        # Display the estimated distance
                        f.write(str(xList[12]) + ' ' + str(yList[12]) + ' ' + str(xList[0]) + ' ' + str(yList[0]) +  ' ' + str(xList[5]) + ' ' + str(yList[5]) + ' ' + str(xList[17]) + ' ' + str(yList[17]) + str(distanceCM) + "\n" )
                        print(f"{int(distanceCM)}cm")
                    except Exception as e:
                        print(f"Error in distance calculation: {e}")

        if hand_results.multi_hand_world_landmarks:
            for hand_landmarks in hand_results.multi_hand_world_landmarks:
                #f.write(str(hand_landmark[0]) + ' ' +str(hand_landmark[1]) + ' ' +  str(hand_landmarks.landmark[9].z*10000) + "\n" )
                #print(hand_landmarks.landmark[9].z*10000)
                time.sleep(0.1)

        #if hand_landmark is not None:
            #send_info([hand_landmark[0], hand_landmark[1], 1], NO_WAIT)
            #print("Sent hand:", hand_landmark_world)

        send_image_yarp(img_out)


def main():
    init()

    # Previous target coordinates
    global prev_target
    send_zero()
    while True:
        follow_hand(1)
        # Exit the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xff == ord('q'):
            break
    close_ports()


if __name__ == "__main__":
    main()
