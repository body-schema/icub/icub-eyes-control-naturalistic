# Strategy 2 - Human face - Human right hand
import random

# !/usr/bin/python3
import yarp
import cv2
import getch
import mediapipe as mp
import numpy as np
import time

# Nose tip landmark index
NOSE_TIP_INDEX = 4


def main():
    # Initialize YARP
    yarp.Network.init()

    mpHands = mp.solutions.hands
    hands = mpHands.Hands(static_image_mode=False, max_num_hands=10, min_detection_confidence=0.5,
                          min_tracking_confidence=0.5)

    # Initialize Mediapipe Face Mesh
    mpdraw = mp.solutions.drawing_utils
    mp_face_mesh = mp.solutions.face_mesh
    face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=5)

    # Open YARP port
    outport = yarp.Port()
    outport.open('/python/gaze_targets:o')
    imagePort = yarp.BufferedPortImageRgb()
    imagePort.open('/mp/image:i')
    image_w = 640
    image_h = 480
    in_buf_array = np.ones((image_h, image_w, 3), dtype=np.uint8)
    in_buf_image = yarp.ImageRgb()
    in_buf_image.resize(image_w, image_h)
    in_buf_image.setExternal(in_buf_array, in_buf_array.shape[1], in_buf_array.shape[0])

    # Connect to YARP port
    torso_port = "/gazeCtrl/in:i"
    yarp.Network.connect(outport.getName(), torso_port)
    yarp.Network.connect('/icub/camcalib/left/out', imagePort.getName())

    output_port = yarp.Port()
    output_port.open("/mediapipe/rgbImage:o")

    img_array = np.zeros((image_h, image_w, 3), dtype=np.uint8)
    yarp_image = yarp.ImageRgb()
    yarp_image.resize(image_w, image_h)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])

    # Previous target coordinates
    prev_target = None
    unchanged_frames = 0

    bot = yarp.Bottle()
    bot.addFloat64(320)
    bot.addFloat64(240)
    bot.addFloat64(0)
    outport.write(bot)
    i = 0

    # Main loop to process video frames
    while True:
        i += 1

        received_image = imagePort.read()
        in_buf_image.copy(received_image)
        image = cv2.cvtColor(in_buf_array, cv2.COLOR_BGR2RGB)

        # Process the frame with Mediapipe Face Mesh
        hand_results = hands.process(image)
        face_mesh_results = face_mesh.process(image)

        h, w, c = image.shape  # Get the height, width, and number of channels of the frame
        img_out = image
        hand_landmark = None
        face_landmark = None

        # Process each detected hand in the frame
        if hand_results.multi_hand_landmarks:
            for handType, handLms in zip(hand_results.multi_handedness, hand_results.multi_hand_landmarks):
                # Extract landmark points and store them in lists
                xList, yList = [], []
                for id, lm in enumerate(handLms.landmark):
                    px, py = int(lm.x * w), int(lm.y * h)
                    xList.append(px)
                    yList.append(py)

                # Draw landmarks and bounding box on the frame
                mpdraw.draw_landmarks(img_out, handLms, mpHands.HAND_CONNECTIONS)
                if handType.classification[0].score > 0.3:
                    hand_landmark = [xList[5], yList[5]]

        # Face mesh detection
        if face_mesh_results.multi_face_landmarks:
            for face_landmarks in face_mesh_results.multi_face_landmarks:
                # Get the coordinates of the nose tip
                nose_tip = face_landmarks.landmark[NOSE_TIP_INDEX]
                nose_tip_x, nose_tip_y = int(nose_tip.x * w), int(nose_tip.y * h)
                face_landmark = [nose_tip_x, nose_tip_y]
                mpdraw.draw_landmarks(img_out, face_landmarks, mp_face_mesh.FACEMESH_CONTOURS)

        if hand_landmark is not None and i < 50:
            bot = yarp.Bottle()
            bot.addFloat64(hand_landmark[0])
            bot.addFloat64(hand_landmark[1])
            bot.addFloat64(1)  # Assuming z-coordinate is 0
            outport.write(bot)
            print("Sent:", hand_landmark)
            time.sleep(0.1)
        elif face_landmark is not None and i < 120:
            bot = yarp.Bottle()
            bot.addFloat64(face_landmark[0])
            bot.addFloat64(face_landmark[1])
            bot.addFloat64(1)  # Assuming z-coordinate is 0
            outport.write(bot)
            print("Sent:", face_landmark)
            time.sleep(0.1)
        else:
            i = 0

        img_array[:] = cv2.cvtColor(img_out, cv2.COLOR_RGB2BGR)
        output_port.write(yarp_image)

        # Exit the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    # Close the YARP port and the network
    outport.close()
    imagePort.close()
    yarp.Network.fini()


if __name__ == "__main__":
    main()
