# !/usr/bin/python3
import yarp
import cv2
import getch
import mediapipe as mp
import numpy as np
import time
import random
from pygame import mixer

# Nose tip landmark index
NOSE_TIP_INDEX = 4
SHOULDER_INDEX = 11
HAND_INDEX = 16

TALK_NOW = 1
DONT_TALK_NOW = 0

LONG_WAIT = 5
SHORT_WAIT = 10
NO_WAIT = 0

FOLLOW_NOTHING = 2
FOLLOW_FACE = 1
FOLLOW_HAND = 0

CENTER_X = 320
CENTER_Y = 240
IMAGE_W = 640
IMAGE_H = 480

DETECTED_FACE = 0
DETECTED_BODY = 1
DETECTED_HAND = 2

SEND_BANANA = SEND_TABLE = 0
SEND_RUBIC = 1
SEND_FORK = 2

START_JOINT_COORDINATES = [-8, -23]
SECOND_START_JOINT_COORDINATES = [-14, -18]
THIRD_START_JOINT_COORDINATES = [10, -16]
FORTH_START_JOINT_COORDINATES = [10, -10]
TABLE_JOINT_COORDINATES = [-25, 0]
FORK_JOINT_COORDINATES = [-25, 20]
RUBIC_JOINT_COORDINATES = [-24, -19]
CIRCLE_JOINT_COORDINATES_X = [16, 16, 16, 16, 16, 16, 16, 13, 6, -2, -7, -13, -13, -14, -14, -14, -14, -14, -14, -11,
                              -7, -1, 6]
CIRCLE_JOINT_COORDINATES_Y = [-19, -12, -7, -1, 6, 14, 19, 19, 19, 19, 20, 20, 13, 9, 3, -2, -8, -15, -20, -20, -20,
                              -20, -20]

CANT_SEE_YOU_AUDIO = "audio_kid/I_can_t_see_your_fac.mp3"

emotion_port = yarp.Port()
say_port = yarp.Port()
outport = yarp.Port()
imagePort = yarp.BufferedPortImageRgb()
output_port = yarp.Port()
torso_port = "/gazeCtrl/in:i"
#emotion_out = "/icub/face/emotions/in"
body_info_port = yarp.Port()

prev_target = None
is_first = False

in_buf_array = np.ones((IMAGE_H, IMAGE_W, 3), dtype=np.uint8)
in_buf_image = yarp.ImageRgb()

img_array = np.zeros((IMAGE_H, IMAGE_W, 3), dtype=np.uint8)
yarp_image = yarp.ImageRgb()

# Initialize Mediapipe Face Mesh
mpdraw = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
mp_drawing_styles = mp.solutions.drawing_styles
holistic = mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5)
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode=False, max_num_hands=10, min_detection_confidence=0.5,
                      min_tracking_confidence=0.5)


def init():
    # Initialize YARP
    yarp.Network.init()

    # Open YARP port
    outport.open('/python/gaze_targets:o')
    imagePort.open('/mp/image:i')
    in_buf_image.resize(IMAGE_W, IMAGE_H)
    in_buf_image.setExternal(in_buf_array, in_buf_array.shape[1], in_buf_array.shape[0])

    # Connect to YARP port
    yarp.Network.connect(outport.getName(), torso_port)
    yarp.Network.connect('/icub/camcalib/right/out', imagePort.getName())

    output_port.open("/mediapipe/rgbImage:o")
    #emotion_port.open("/python/emotions:o")
    say_port.open("/python/say_now:o")
    body_info_port.open("boy_joints:i")
    yarp.Network.connect("/icub/head/state:o", body_info_port.getName())
    yarp.Network.connect(outport.getName(), torso_port)


    #yarp.Network.connect(emotion_port.getName(), emotion_out)
    yarp_image.resize(IMAGE_W, IMAGE_H)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])


def close_ports():
    # Close the YARP port and the network
    outport.close()
    imagePort.close()
    yarp.Network.fini()


def draw_face_landmarks(img_out, results):
    mpdraw.draw_landmarks(
        img_out,
        results.face_landmarks,
        mp_holistic.FACEMESH_TESSELATION,
        landmark_drawing_spec=None,
        connection_drawing_spec=mp_drawing_styles.get_default_face_mesh_tesselation_style())


def draw_pose_landmarks(img_out, results):
    mpdraw.draw_landmarks(
        img_out,
        results.pose_landmarks,
        mp_holistic.POSE_CONNECTIONS,
        landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())


def should_move(nose_tip_x, nose_tip_y):
    return prev_target is not None and (abs(prev_target[0] - nose_tip_x) >= 5 or abs(prev_target[1] - nose_tip_y) >= 5)


def send_image_yarp(img_out):
    img_array[:] = cv2.cvtColor(img_out, cv2.COLOR_RGB2BGR)
    output_port.write(yarp_image)


def send_nose_coordinates(results, w, h):
    global prev_target
    for face_landmark in results.face_landmarks.landmark:
        # Get the coordinates of the nose tip
        nose_tip = face_landmark
        nose_tip_x, nose_tip_y = int(nose_tip.x * w), int(nose_tip.y * h)

        # Send nose tip coordinates to YARP port only if x or y has changed by more than 5 pixels
        current_target = [nose_tip_x, nose_tip_y, 1]
        if should_move(nose_tip_x, nose_tip_y):
            send_info([nose_tip_x, nose_tip_y, 1], NO_WAIT)
            print("Sent face:", current_target)
            time.sleep(0.1)
        elif prev_target is None:
            send_info([nose_tip_x, nose_tip_y, 1], NO_WAIT)
            print("Sent face:", current_target)
            prev_target = current_target
        break


def send_pose_coordinates(results, w, h, index):
    global prev_target
    for j in range(index + 1):
        # Get the coordinates of shoulders
        if j != index:
            continue
        shoulder_coordinates = results.pose_landmarks.landmark[j]
        shoulder_coordinates_x, shoulder_coordinates_y = int(shoulder_coordinates.x * w), int(
            shoulder_coordinates.y * h)

        # Send nose tip coordinates to YARP port only if x or y has changed by more than 5 pixels
        current_target = [shoulder_coordinates_x, shoulder_coordinates_y, 1]
        if should_move(shoulder_coordinates_x, shoulder_coordinates_y):
            send_info([shoulder_coordinates_x, shoulder_coordinates_y, 1], NO_WAIT)
            print("Sent pose:", current_target)
            time.sleep(0.1)
        elif prev_target is None:
            send_info([shoulder_coordinates_x, shoulder_coordinates_y, 1], NO_WAIT)
            print("Sent pose:", current_target)
            prev_target = current_target


# Random joint coordinates
def random_pixel():
    return random.randint(-25, 10), random.randint(-20, 20)


def wait(timer):
    # Wait for a while before sending the next random location
    for k in range(timer):
        time.sleep(0.1)
        received_image = imagePort.read()
        output_port.write(received_image)


def send_info(info, timer):
    bot = yarp.Bottle()
    for obj in info:
        bot.addFloat64(obj)
    outport.write(bot)
    bot.clear()
    wait(timer)


def open_mouth():
    bot = yarp.Bottle()
    bot.clear()
    bot.addString("set")
    bot.addString("mou")
    bot.addString("sur")
    emotion_port.write(bot)


def close_mouth():
    bot = yarp.Bottle()
    bot.clear()
    bot.addString("set")
    bot.addString("mou")
    bot.addString("neu")
    emotion_port.write(bot)

def send_say_signal(code):
    bot = yarp.Bottle()
    bot.clear()
    bot.addInt32(code)
    say_port.write(bot)


def blink():
    # blink
    send_info([0], SHORT_WAIT)
    print("Blink")


def send_random_coordinates():
    # Generate random pixel location
    random_x, random_y = random_pixel()

    # Send random pixel coordinates to YARP port
    send_info([random_x, random_y], LONG_WAIT)
    print("Sent random:", [random_x, random_y])


def send_zero():
    send_info([CENTER_X, CENTER_Y, 0], SHORT_WAIT)
    print("Sent zero")


def send_table(must_send):
    if must_send == SEND_TABLE or must_send == SEND_BANANA:
        send_info(TABLE_JOINT_COORDINATES, LONG_WAIT)
    elif must_send == SEND_RUBIC:
        send_info(RUBIC_JOINT_COORDINATES, LONG_WAIT)
    elif must_send == SEND_FORK:
        send_info(FORK_JOINT_COORDINATES, LONG_WAIT)
    print("Sent table")


def send_circle():
    for (x, y) in zip(CIRCLE_JOINT_COORDINATES_X, CIRCLE_JOINT_COORDINATES_Y):
        send_info([x, y], SHORT_WAIT)
    print("Sent circle")


def follow_face(timer):
    ret = DETECTED_HAND
    for k in range(timer):
        received_image = imagePort.read()
        in_buf_image.copy(received_image)
        image = cv2.cvtColor(in_buf_array, cv2.COLOR_BGR2RGB)

        # Process the frame with Mediapipe Face Mesh
        results = holistic.process(image)

        h, w, c = image.shape  # Get the height, width, and number of channels of the frame
        img_out = image

        # Face mesh detection
        if results.pose_landmarks:
            if results.face_landmarks is not None:
                send_nose_coordinates(results, w, h)
                ret = DETECTED_FACE
            elif results.pose_landmarks is not None:
                send_pose_coordinates(results, w, h, SHOULDER_INDEX)
                ret = DETECTED_BODY
            else:
                ret = DETECTED_HAND

            draw_face_landmarks(img_out, results)
            draw_pose_landmarks(img_out, results)
        else:
            ret = DETECTED_HAND

        send_image_yarp(img_out)
    if ret == DETECTED_HAND and not is_first:
        send_random_coordinates()
    elif ret == DETECTED_HAND and is_first:
        send_info(SECOND_START_JOINT_COORDINATES, SHORT_WAIT)
    return ret


def follow_hand(timer):
    global prev_target
    ret = DETECTED_HAND
    for k in range(timer):
        received_image = imagePort.read()
        in_buf_image.copy(received_image)
        image = cv2.cvtColor(in_buf_array, cv2.COLOR_BGR2RGB)

        hand_results = hands.process(image)
        body_results = holistic.process(image)

        h, w, c = image.shape  # Get the height, width, and number of channels of the frame
        img_out = image

        hand_landmark = None

        # Process each detected hand in the frame
        if hand_results.multi_hand_landmarks:
            ret = DETECTED_BODY
            for handType, handLms in zip(hand_results.multi_handedness, hand_results.multi_hand_landmarks):
                # Extract landmark points and store them in lists
                xList, yList = [], []
                for id, lm in enumerate(handLms.landmark):
                    px, py = int(lm.x * w), int(lm.y * h)
                    xList.append(px)
                    yList.append(py)

                # Draw landmarks and bounding box on the frame
                mpdraw.draw_landmarks(img_out, handLms, mpHands.HAND_CONNECTIONS)
                if handType.classification[0].score > 0.3:
                    hand_landmark = [xList[5], yList[5]]
        elif body_results.pose_landmarks:
            send_pose_coordinates(body_results, w, h, HAND_INDEX)
        else:
            ret = DETECTED_HAND

        if hand_landmark is not None:
            send_info([hand_landmark[0], hand_landmark[1], 1], NO_WAIT)
            prev_target = [hand_landmark[0], hand_landmark[1]]
            print("Sent hand:", hand_landmark)

        send_image_yarp(img_out)
    if ret == DETECTED_HAND and not is_first:
        send_random_coordinates()
    elif ret == DETECTED_HAND and is_first:
        send_info(SECOND_START_JOINT_COORDINATES, SHORT_WAIT)
    return ret


def play_cant_see_you():
    time.sleep(0.5)
    send_say_signal(TALK_NOW)
    mixer.init()
    # say_mouth()
    mixer.music.load(CANT_SEE_YOU_AUDIO)
    mixer.music.play()
    i = 0
    # say_mouth()
    i_bound = random.randint(0, 10)
    while mixer.music.get_busy():
        # say_mouth()
        i += 1
        if i == i_bound:
            blink()
            i = 0
            # say_mouth()
            i_bound = random.randint(0, 10)
        follow_face(1)
        # say_mouth()
    send_say_signal(DONT_TALK_NOW)


def say_mouth():
    open_mouth()
    curr = time.time()
    while (time.time() < curr + 0.4):
        pass
    close_mouth()


def play_sound(audio, follow_what, randomness_bound=1000):
    mixer.init()
    mixer.music.load(audio)
    mixer.music.play()
    is_face_detected = -1
    i_blink = 0
    i_rand = 0
    i_bound_blink = random.randint(2, 15)
    i_bound_random = random.randint(randomness_bound // 2, 100 + randomness_bound)
    while mixer.music.get_busy():  # wait for music to finish playing
        send_say_signal(TALK_NOW)
        # say_mouth()
        if is_first and i_blink % 5 == 0 and prev_target is not None:
            send_zero()
            # say_mouth()
            send_info([prev_target[0], prev_target[1], 1], LONG_WAIT)
        elif is_first and i_blink % 5 == 0:
            send_zero()
            # say_mouth()z
            send_random_coordinates()
        i_blink += 1
        i_rand += 1
        if i_blink == i_bound_blink and i_blink % 2:
            blink()
            # say_mouth()
            i_blink = 0
            i_bound_blink = random.randint(1, 15)
        if i_rand == i_bound_random and randomness_bound < 100:
            # say_mouth()
            send_random_coordinates()
            i_rand = 0
            i_bound_random = random.randint(randomness_bound // 2, 100 + randomness_bound)
        # say_mouth()
        if follow_what == FOLLOW_FACE:
            is_face_detected = follow_face(1)
        elif follow_what == FOLLOW_HAND:
            is_face_detected = follow_hand(1)
        elif is_face_detected == DETECTED_HAND and not is_first:
            send_random_coordinates()
        elif is_face_detected == DETECTED_HAND and is_first:
            send_info(SECOND_START_JOINT_COORDINATES, SHORT_WAIT)
            send_info(START_JOINT_COORDINATES, SHORT_WAIT)
        # say_mouth()
    send_say_signal(DONT_TALK_NOW)

    if is_first and (is_face_detected == DETECTED_BODY or is_face_detected == DETECTED_HAND):
        # say_mouth()
        play_cant_see_you()

def is_human_in_front():
    bot = yarp.Bottle()
    body_info_port.read(bot)
    return (follow_face(20) == DETECTED_FACE or follow_face(20) == DETECTED_BODY) and abs(bot.get(4).asInt32()) <= 6


def start_find():
    send_info(START_JOINT_COORDINATES, SHORT_WAIT)
    if follow_face(2) == DETECTED_HAND:
        send_info(SECOND_START_JOINT_COORDINATES, SHORT_WAIT)
        if follow_face(2) == DETECTED_HAND:
            send_info(THIRD_START_JOINT_COORDINATES, SHORT_WAIT)
            if follow_face(2) == DETECTED_HAND:
                send_info(FORTH_START_JOINT_COORDINATES, SHORT_WAIT)
                if follow_face(2) == DETECTED_HAND:
                    play_cant_see_you()
                    if not is_human_in_front():
                        play_cant_see_you()
                    else:
                        play_sound("new_audio/Good_Now_I_can_see_y.mp3", FOLLOW_FACE, randomness_bound=5000)



def scenario_two():
    global is_first
    # Face / stul -> Face / ruka
    send_table(SEND_TABLE)
    time.sleep(1)
    send_zero()
    play_sound("new_audio/Hello_And_welcome_to.mp3", FOLLOW_FACE, randomness_bound=2000)
    start_find()
    is_first = False
    time.sleep(1)
    blink()
    play_sound("new_audio/I_m_excited_to_engag.mp3", FOLLOW_FACE, randomness_bound=2000)
    time.sleep(1)
    play_sound("audio_kid/Let_s_take_a_look_at.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    send_table(SEND_TABLE)
    blink()
    play_sound("new_audio/Here_we_have_a_three.mp3", FOLLOW_NOTHING, randomness_bound=5000)
    send_zero()
    blink()
    send_table(SEND_TABLE)
    play_sound("new_audio/Banana.mp3", FOLLOW_NOTHING, randomness_bound=5000)
    blink()
    send_table(SEND_FORK)
    play_sound("audio_kid/Fork.mp3", FOLLOW_NOTHING, randomness_bound=5000)
    #send_zero()
    blink()
    send_table(SEND_RUBIC)
    play_sound("audio_kid/And_the_Rubic_cube.mp3", FOLLOW_NOTHING, randomness_bound=5000)
    blink()
    send_zero()
    play_sound("audio_kid/Now_let_s_play_a_gam.mp3", FOLLOW_FACE, randomness_bound=2000)
    send_table(SEND_TABLE)
    blink()
    follow_hand(60)
    send_zero()
    if prev_target is not None:
        send_info([prev_target[0], prev_target[1], 1], LONG_WAIT)
    else:
        send_table(SEND_TABLE)
    follow_hand(60)
    send_zero()
    blink()
    play_sound("audio_kid/Okay_you_re_doing_gr.mp3", FOLLOW_FACE, randomness_bound=2000)
    send_table(SEND_TABLE)
    blink()
    follow_hand(60)
    send_zero()
    if prev_target is not None:
        send_info([prev_target[0], prev_target[1], 1], LONG_WAIT)
    else:
        send_table(SEND_TABLE)
    follow_hand(60)
    blink()
    send_zero()
    play_sound("audio_kid/And_lastly_where_is.mp3", FOLLOW_FACE, randomness_bound=2000)
    send_table(SEND_TABLE)
    blink()
    follow_hand(60)
    send_zero()
    if prev_target is not None:
        send_info([prev_target[0], prev_target[1], 1], LONG_WAIT)
    else:
        send_table(SEND_TABLE)
    follow_hand(60)
    blink()
    send_zero()
    play_sound("audio_kid/Thank_you_You_re_doi.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    play_sound("audio_kid/Thank_you_for_partic.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    send_zero()


def scenario_three():
    # Face -> Ruka
    send_table(SEND_TABLE)
    time.sleep(1)
    send_zero()
    play_sound("new_audio/Hello_And_welcome_to.mp3", FOLLOW_FACE, randomness_bound=2000)
    start_find()
    time.sleep(1)
    blink()
    play_sound("new_audio/I_m_excited_to_engag.mp3", FOLLOW_FACE, randomness_bound=2000)
    time.sleep(1)
    play_sound("audio_kid/Let_s_take_a_look_at.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    play_sound("new_audio/Here_we_have_a_three.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    play_sound("new_audio/Banana.mp3", FOLLOW_FACE, randomness_bound=2000)
    play_sound("audio_kid/Fork.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    play_sound("audio_kid/And_the_Rubic_cube.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    send_zero()
    play_sound("audio_kid/Now_let_s_play_a_gam.mp3", FOLLOW_FACE, randomness_bound=2000)

    send_table(SEND_TABLE)
    blink()
    follow_hand(180)
    blink()
    play_sound("audio_kid/Okay_you_re_doing_gr.mp3", FOLLOW_HAND, randomness_bound=2000)
    send_table(SEND_TABLE)
    follow_hand(180)
    blink()
    play_sound("audio_kid/And_lastly_where_is.mp3", FOLLOW_HAND, randomness_bound=2000)
    send_table(SEND_TABLE)
    follow_hand(180)
    blink()

    send_zero()
    play_sound("audio_kid/Thank_you_You_re_doi.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    play_sound("audio_kid/Thank_you_for_partic.mp3", FOLLOW_FACE, randomness_bound=2000)
    blink()
    send_zero()


def scenario_one():
    # Random -> Random
    send_table(SEND_TABLE)
    time.sleep(1)
    send_zero()
    play_sound("new_audio/Hello_And_welcome_to.mp3", FOLLOW_NOTHING, randomness_bound=10)
    start_find()
    time.sleep(1)
    play_sound("new_audio/I_m_excited_to_engag.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(1)
    play_sound("audio_kid/Let_s_take_a_look_at.mp3", FOLLOW_NOTHING, randomness_bound=10)
    play_sound("new_audio/Here_we_have_a_three.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(1)
    play_sound("new_audio/Banana.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(1)
    play_sound("audio_kid/Fork.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(1)
    play_sound("audio_kid/And_the_Rubic_cube.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(1)
    play_sound("audio_kid/Now_let_s_play_a_gam.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(6)
    play_sound("audio_kid/Okay_you_re_doing_gr.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(6)
    play_sound("audio_kid/And_lastly_where_is.mp3", FOLLOW_NOTHING, randomness_bound=10)
    time.sleep(6)
    play_sound("audio_kid/Thank_you_You_re_doi.mp3", FOLLOW_NOTHING, randomness_bound=10)
    play_sound("audio_kid/Thank_you_for_partic.mp3", FOLLOW_NOTHING, randomness_bound=10)
    send_zero()


def main():
    init()
    #scenario_one()
    scenario_two()
    #scenario_three()

    mixer.music.load("new_audio/Here_we_have_a_three.mp3", FOLLOW_FACE, is_first=False)
    close_ports()


if __name__ == "__main__":
    main()
