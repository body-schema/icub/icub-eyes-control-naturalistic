# Strategy 4 - Hum6an - Table - Random

# !/usr/bin/python3
import yarp
import cv2
import getch
import mediapipe as mp
import numpy as np
import time
import random

# Nose tip landmark index
NOSE_TIP_INDEX = 4


# Random pixel coordinates
def random_pixel():
    return random.randint(-25, 10), random.randint(-20, 20)


def main():
    # Initialize YARP
    yarp.Network.init()

    # Initialize Mediapipe Face Mesh
    mpdraw = mp.solutions.drawing_utils
    mp_face_mesh = mp.solutions.face_mesh
    face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.3, min_tracking_confidence=0.3, max_num_faces=1)

    # Open YARP port
    outport = yarp.Port()
    outport.open('/python/gaze_targets:o')
    imagePort = yarp.BufferedPortImageRgb()
    imagePort.open('/mp/image:i')
    image_w = 640
    image_h = 480
    in_buf_array = np.ones((image_h, image_w, 3), dtype=np.uint8)
    in_buf_image = yarp.ImageRgb()
    in_buf_image.resize(image_w, image_h)
    in_buf_image.setExternal(in_buf_array, in_buf_array.shape[1], in_buf_array.shape[0])

    # Connect to YARP port
    torso_port = "/gazeCtrl/in:i"
    yarp.Network.connect(outport.getName(), torso_port)
    yarp.Network.connect('/icub/camcalib/left/out', imagePort.getName())

    output_port = yarp.Port()
    output_port.open("/mediapipe/rgbImage:o")

    img_array = np.zeros((image_h, image_w, 3), dtype=np.uint8)
    yarp_image = yarp.ImageRgb()
    yarp_image.resize(image_w, image_h)
    yarp_image.setExternal(img_array, img_array.shape[1], img_array.shape[0])

    # Previous target coordinates
    prev_target = None
    unchanged_frames = 0

    bot = yarp.Bottle()
    bot.addFloat64(320)
    bot.addFloat64(240)
    bot.addFloat64(0)
    outport.write(bot)
    i = 0
    i_bound = random.randint(1, 1000)

    # Main loop to process video frames
    while True:
        i += 1
        if i == i_bound and i_bound % 2:
            # TODO Here add location of the table
            bot = yarp.Bottle()
            bot.addFloat64(-25)
            bot.addFloat64(0)
            outport.write(bot)

            for i in range(30):
                time.sleep(0.1)
                received_image = imagePort.read()
                output_port.write(received_image)

            i = 0
            i_bound = random.randint(100, 1000)

        if i == i_bound and i_bound % 2 == 0:
            # Generate random pixel location
            random_x, random_y = random_pixel()

            # Send random pixel coordinates to YARP port
            bot = yarp.Bottle()
            bot.addFloat64(random_x)
            bot.addFloat64(random_y)
            outport.write(bot)
            print("Sent:", [random_x, random_y, 1])
            i = 0
            i_bound = random.randint(1, 900)

            # Wait for a while before sending the next random location
            for k in range(30):
                time.sleep(0.1)
                received_image = imagePort.read()
                output_port.write(received_image)

        received_image = imagePort.read()
        in_buf_image.copy(received_image)
        image = cv2.cvtColor(in_buf_array, cv2.COLOR_BGR2RGB)

        # Process the frame with Mediapipe Face Mesh
        face_mesh_results = face_mesh.process(image)

        h, w, c = image.shape  # Get the height, width, and number of channels of the frame
        img_out = image

        # Face mesh detection
        if face_mesh_results.multi_face_landmarks:
            for face_landmarks in face_mesh_results.multi_face_landmarks:
                # Get the coordinates of the nose tip
                nose_tip = face_landmarks.landmark[NOSE_TIP_INDEX]
                nose_tip_x, nose_tip_y = int(nose_tip.x * w), int(nose_tip.y * h)

                # Send nose tip coordinates to YARP port only if x or y has changed by more than 5 pixels
                current_target = [nose_tip_x, nose_tip_y, 1]
                if prev_target is not None and (
                        abs(prev_target[0] - nose_tip_x) >= 5 or abs(prev_target[1] - nose_tip_y) >= 5):
                    bot = yarp.Bottle()
                    bot.addFloat64(nose_tip_x)
                    bot.addFloat64(nose_tip_y)
                    bot.addFloat64(1)  # Assuming z-coordinate is 0
                    outport.write(bot)
                    print("Sent:", current_target)
                    time.sleep(0.1)
                elif prev_target is None:
                    bot = yarp.Bottle()
                    bot.addFloat64(nose_tip_x)
                    bot.addFloat64(nose_tip_y)
                    bot.addFloat64(1)  # Assuming z-coordinate is 0
                    outport.write(bot)
                    print("Sent:", current_target)
                prev_target = current_target

                mpdraw.draw_landmarks(img_out, face_landmarks, mp_face_mesh.FACEMESH_CONTOURS)
        elif prev_target is not None:
            bot = yarp.Bottle()
            bot.addFloat64(prev_target[0])
            bot.addFloat64(prev_target[1])
            bot.addFloat64(1)  # Assuming z-coordinate is 0
            outport.write(bot)

        img_array[:] = cv2.cvtColor(img_out, cv2.COLOR_RGB2BGR)
        output_port.write(yarp_image)

        # Exit the loop if 'q' key is pressed
        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    # Close the YARP port and the network
    outport.close()
    imagePort.close()
    yarp.Network.fini()


if __name__ == "__main__":
    main()
